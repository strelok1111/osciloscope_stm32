#include "adc.h"

volatile uint16_t readed_data_index = 0;
volatile uint16_t read_data_len = 0;
volatile uint16_t *data_pointer;
static void tim_read_adc_setup(void){
	rcc_periph_clock_enable(RCC_TIM8);
	nvic_enable_irq(NVIC_TIM8_UP_IRQ);
	timer_enable_irq(TIM8, TIM_DIER_UIE);
}
void tim8_up_isr(void){
	if (timer_get_flag(TIM8, TIM_SR_UIF)) {
		timer_clear_flag(TIM8, TIM_SR_UIF);
		if(readed_data_index < read_data_len){
			ADC_CLK_TOUCH;
			data_pointer[readed_data_index] = GPIO_IDR(ADC_DATA_PORT);
			readed_data_index++;
		}else{
			timer_disable_counter(TIM8);
		}

	}
}
void adc_gpio_setup(void){
	rcc_periph_clock_enable(RCC_GPIOA);
	rcc_periph_clock_enable(RCC_GPIOB);
	gpio_mode_setup(ADC_CLK_PORT, GPIO_MODE_OUTPUT,GPIO_PUPD_PULLUP, ADC_CLK_PIN);
	gpio_set_output_options(ADC_CLK_PORT,GPIO_OTYPE_PP,GPIO_OSPEED_50MHZ,ADC_CLK_PIN);
	gpio_set(ADC_CLK_PORT,ADC_CLK_PIN);
	gpio_clear(ADC_CLK_PORT,GPIO4);
	gpio_mode_setup(ADC_DATA_PORT, GPIO_MODE_INPUT,GPIO_PUPD_NONE, GPIO0 | GPIO1 | GPIO2 | GPIO3 | GPIO4 | GPIO5 | GPIO6 | GPIO7 | GPIO8 | GPIO9);
	tim_read_adc_setup();
}
void adc_convert_data_to_10(uint16_t len,uint16_t *data){
	for(uint16_t i = 0;i < len;i++)
		data[i] &= 0x03FF;
}

void adc_read_data(uint32_t del,uint16_t len,uint16_t *data){
	for(uint16_t i = 0;i < len;i++){
		ADC_CLK_TOUCH;
		data[i] = GPIO_IDR(ADC_DATA_PORT);
		for (uint32_t d = 0; d < del; d++) __asm__("nop");
	}
	adc_convert_data_to_10(len,data);
}
void adc_shift_data(uint16_t len,uint16_t *data,uint8_t shift){
	uint16_t temp_data[len];
	memcpy(temp_data,data,len * 2);
	for(uint16_t i = 0;i < len;i++){
		data[i] = temp_data[i/shift];
	}
}
void adc_read_min_delay(uint16_t len,uint16_t *data){
	adc_read_20ms(len,data);
}
void adc_read_05us(uint16_t len,uint16_t *data){
	READ_FAST;
	adc_convert_data_to_10(len,data);
	adc_shift_data(len,data,4);
}
void adc_read_1us(uint16_t len,uint16_t *data){
	READ_FAST;
	adc_convert_data_to_10(len,data);
	adc_shift_data(len,data,2);
}
void adc_read_2_5us(uint16_t len,uint16_t *data){
	READ_FAST;
	adc_convert_data_to_10(len,data);
	adc_shift_data(len,data,1);
}
void adc_read_5us(uint16_t len,uint16_t *data){
	READ_FAST2;
	adc_convert_data_to_10(len,data);
}
void adc_read_10us(uint16_t len,uint16_t *data){
	READ_FAST3;
	adc_convert_data_to_10(len,data);
}
void adc_read_50us(uint16_t len,uint16_t *data){
	adc_read_data(6,len,data);
}
void adc_read_0_1ms(uint16_t len,uint16_t *data){
	timer_set_prescaler(TIM8, 0);
	timer_set_period(TIM8,162);
	data_pointer = data;
	readed_data_index = 0;
	read_data_len = len;
	timer_enable_counter(TIM8);
	while(readed_data_index < len);
	adc_convert_data_to_10(len,data);
}
void adc_read_0_5ms(uint16_t len,uint16_t *data){
	timer_set_prescaler(TIM8, 0);
	timer_set_period(TIM8,817);
	data_pointer = data;
	readed_data_index = 0;
	read_data_len = len;
	timer_enable_counter(TIM8);
	while(readed_data_index < len);
	adc_convert_data_to_10(len,data);
}
void adc_read_1ms(uint16_t len,uint16_t *data){
	timer_set_prescaler(TIM8, 0);
	timer_set_period(TIM8,1635);
	data_pointer = data;
	readed_data_index = 0;
	read_data_len = len;
	timer_enable_counter(TIM8);
	while(readed_data_index < len);
	adc_convert_data_to_10(len,data);
}
void adc_read_10ms(uint16_t len,uint16_t *data){
	timer_set_prescaler(TIM8, 0);
	timer_set_period(TIM8,16362);
	data_pointer = data;
	readed_data_index = 0;
	read_data_len = len;
	timer_enable_counter(TIM8);
	while(readed_data_index < len);
	adc_convert_data_to_10(len,data);
}
void adc_read_20ms(uint16_t len,uint16_t *data){
	timer_set_prescaler(TIM8, 0);
	timer_set_period(TIM8,32726);
	data_pointer = data;
	readed_data_index = 0;
	read_data_len = len;
	timer_enable_counter(TIM8);
	while(readed_data_index < len);
	adc_convert_data_to_10(len,data);
}

