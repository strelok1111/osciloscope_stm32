#include <stdint.h>
#include <string.h>
#include <stdlib.h>
#include <libopencm3/stm32/gpio.h>
#include <libopencm3/stm32/rcc.h>

#define ADA_LCD_CS_PIN    GPIO13
#define ADA_LCD_CS_PORT   GPIOC
#define ADA_LCD_RS_PIN    GPIO14
#define ADA_LCD_RS_PORT   GPIOC
#define ADA_LCD_WR_PIN    GPIO15
#define ADA_LCD_WR_PORT   GPIOC
#define ADA_LCD_RD_PIN    GPIO0
#define ADA_LCD_RD_PORT   GPIOA
#define ADA_LCD_RST_PIN   GPIO15
#define ADA_LCD_RST_PORT  GPIOA

#define ADA_LCD_WR_PIN_CLEAR ADA_LCD_WR_PIN << 16

#define ADA_LCD_D0_PIN  GPIO6
#define ADA_LCD_D0_PORT GPIOA
#define ADA_LCD_D1_PIN  GPIO5
#define ADA_LCD_D1_PORT GPIOA
#define ADA_LCD_D2_PIN  GPIO14
#define ADA_LCD_D2_PORT GPIOB
#define ADA_LCD_D3_PIN  GPIO13
#define ADA_LCD_D3_PORT GPIOB
#define ADA_LCD_D4_PIN  GPIO12
#define ADA_LCD_D4_PORT GPIOB
#define ADA_LCD_D5_PIN  GPIO11
#define ADA_LCD_D5_PORT GPIOB
#define ADA_LCD_D6_PIN  GPIO10
#define ADA_LCD_D6_PORT GPIOB
#define ADA_LCD_D7_PIN  GPIO7
#define ADA_LCD_D7_PORT GPIOA


#define TFTLCD_DELAY 0xFF


#define ILI932X_GRAM_HOR_AD        0x20
#define ILI932X_GRAM_VER_AD        0x21
#define ILI932X_RW_GRAM            0x22
#define ILI932X_HOR_START_AD       0x50
#define ILI932X_HOR_END_AD         0x51
#define ILI932X_VER_START_AD       0x52
#define ILI932X_VER_END_AD         0x53

#define RD_ACTIVE    { gpio_clear(ADA_LCD_RD_PORT,ADA_LCD_RD_PIN); }
#define RD_IDLE      { gpio_set(ADA_LCD_RD_PORT,ADA_LCD_RD_PIN); }
#define WR_ACTIVE    { gpio_clear(ADA_LCD_WR_PORT,ADA_LCD_WR_PIN); }
#define WR_IDLE      { gpio_set(ADA_LCD_WR_PORT,ADA_LCD_WR_PIN); }
#define CD_COMMAND   { gpio_clear(ADA_LCD_RS_PORT,ADA_LCD_RS_PIN); }
#define CD_DATA      { gpio_set(ADA_LCD_RS_PORT,ADA_LCD_RS_PIN); }
#define CS_ACTIVE    { gpio_clear(ADA_LCD_CS_PORT,ADA_LCD_CS_PIN); }
#define CS_IDLE      { gpio_set(ADA_LCD_CS_PORT,ADA_LCD_CS_PIN); }
#define RESET_0      { gpio_clear(ADA_LCD_RST_PORT,ADA_LCD_RST_PIN); }
#define RESET_1      { gpio_set(ADA_LCD_RST_PORT,ADA_LCD_RST_PIN); }


#define WR_STROBE { GPIO_BSRR(ADA_LCD_WR_PORT) = ADA_LCD_WR_PIN_CLEAR;__asm__("nop");__asm__("nop"); GPIO_BSRR(ADA_LCD_WR_PORT) = ADA_LCD_WR_PIN;}

#define XSIZE 240
#define YSIZE 320
#define XMAX XSIZE-1
#define YMAX YSIZE-1
#define XLAND_MAX YMAX
#define YLAND_MAX XMAX

#define BLACK 0x0000
#define BLUE 0x1919
#define RED 0xe0e0
#define GREEN 0x2727
#define LIME  0xc7c7
#define CYAN 0x1f1f
#define MAGENTA 0x9090
#define YELLOW 0xe7e7
#define WHITE 0xFFFF


void ada_lcd_reset(void);
void ada_lcd_begin(void);
void ada_lcd_set_addr_window(uint16_t, uint16_t, uint16_t, uint16_t);
void ada_lcd_fill_rect(uint16_t,uint16_t,uint16_t,uint16_t, uint16_t);
void ada_lcd_clear(void);
void ada_lcd_draw_pixel (uint16_t, uint16_t, uint16_t);
void ada_lcd_draw_line (uint16_t, uint16_t, uint16_t, uint16_t, uint16_t);
void ada_lcd_draw_char(uint16_t, uint16_t,uint8_t, uint16_t);
void ada_lcd_draw_string(uint16_t, uint16_t,char *, uint16_t);
void ada_lcd_draw_rectanlge(uint16_t, uint16_t,uint16_t, uint16_t,uint16_t);
void ada_lcd_draw_int(uint16_t,uint16_t,int32_t,int16_t);



